pub mod simulation;
pub mod models;

use gnuplot::*;
use simulation::{ Simulation, State, Model, euler, improvedeuler };
use models::volterra::{ PredatorPrey, Pair };
use models::climatebalance::{ ClimateBalance };


fn plotclimate (data: Vec<State<f64>>, output: bool) {
    let mut fg = Figure::new();
    // TODO don't create new data on heap, use existing structure with offset and alignment
    let x: Vec<f64> = data.iter().map(|euler| euler.t * 3.1709792*10e-8).collect();
    let y: Vec<f64> = data.iter().map(|euler| euler.y).collect();
    fg.axes2d().lines(&x, &y, &[Caption("A line"), Color("black"), ])
        .set_title("Energy Balance Model", &[]);

    fg.show();
    if output {
        fg.set_terminal("svg", "./result.svg");
        fg.show();
    }
}

fn plotvolterra (data: Vec<State<Pair>>, output: bool) {
    let mut fg = Figure::new();
    // TODO don't create new data on heap, use existing structure with offset and alignment
    let x: Vec<f64> = data.iter().map(|euler| euler.t).collect();
    let prey: Vec<f64> = data.iter().map(|euler| euler.y.prey).collect();
    let predator: Vec<f64> = data.iter().map(|euler| euler.y.predator).collect();
    fg.axes2d().lines(&x, &predator, &[Caption("Predator"), Color("green"), ]).lines(&x, &prey, &[Caption("Prey"), Color("red"), ])
        .set_title("Predator Prey", &[]);

    fg.show();
    if output {
        fg.set_terminal("svg", "./result.svg");
        fg.show();
    }
}

fn compareintegrators <M: Model<f64>, P>(sim: &Simulation<f64, M>, functions: Vec<P>)
    where P: Fn(&M, &State<f64>, f64) -> f64 {
    let mut fg = Figure::new();
    let colors = ["blue", "red", "green", "yellow", "purple", "brown"];
    let mut i = 0;

    for f in functions {
        let data = sim.run(&f);
        let x: Vec<f64> = data.iter().map(|euler| euler.t * 3.1709792*10e-8).collect();
        let y: Vec<f64> = data.iter().map(|euler| euler.y).collect();
        fg.axes2d().lines(&x, &y, &[Caption("A line"), Color(colors[i % colors.len()])]);
        i += 1;
    }
    // fg.axes2d().set_title("Energy Balance Model", &[]);

    fg.show();
}

fn main() {
    // setup climate simulation
    let climatemodel       = ClimateBalance::new(1367.0, 9.96e6, 0.3, 0.62, 5.67e-8);
    let climatesimulation  = Simulation::new(State{t: 0.0, y: 200.0},
                                                       100000000.0,
                                                       30, // Number of steps
                                                       climatemodel);

    // setup predator-prey simulatio
    let predatormodel      = PredatorPrey::new(1.0, 0.2, 0.2*0.5, 0.2);
    let predatorsimulation = Simulation::new(State {t: 0.0, y: Pair{prey: 1 as f64, predator: 1.5 as f64}},
                                                 100.0,
                                                 10000,
                                                 predatormodel);
    // COMPARE Different Integrators
    let mut integrators = Vec::new();
    integrators.push(euler as fn(&ClimateBalance, &State<f64>, f64) -> f64);
    integrators.push(improvedeuler as fn(&ClimateBalance, &State<f64>, f64) -> f64);
    compareintegrators(&climatesimulation, integrators);

    // Lotka-Volterra
    let data = predatorsimulation.run(&improvedeuler);
    plotvolterra(data, false);

}

use std::ops::{Add, Mul};

#[derive(Clone, Copy)]
pub struct State<T: Copy + Clone> {
    pub t: f64,
    pub y: T,
}

pub struct Simulation<T: Copy + Clone, M: Model<T>> {
   params: Parameters<T>,
   model: M,
}

#[derive(Clone, Copy)]
struct Parameters<T: Copy + Clone> {
   initial: State<T>,
   endtime: f64,
   steps: usize,
}

impl <T: Copy + Clone, M: Model<T>> Simulation<T, M> {
    pub fn new(initial: State<T>, endtime: f64, steps: usize, model: M) -> Self {
        Simulation {params: Parameters{initial: initial, endtime: endtime, steps: steps}, model: model}
    }
    pub fn run<P> (&self, integrator: &P) -> Vec<State<T>>
        where P: Fn(&M, &State<T>, f64) -> T {
        let dt: f64 = self.params.endtime/(self.params.steps as f64);
        let mut arr: Vec<State<T>> = Vec::with_capacity(self.params.steps);
        arr.insert(0, self.params.initial);
        for k in 0..self.params.steps {
            arr.insert(k+1, State{t: arr[k].t + dt, y: integrator(&self.model, &arr[k], dt)});
        }
        arr
    }
}

pub trait Model<T: Copy + Clone> {
    fn modelfun(&self, s: &State<T>) -> T;
}

pub fn euler<T: Add<Output = T> + Mul<f64, Output=T> + Copy, M: Model<T>>(model: &M, s: &State<T>, dt: f64) -> T {
    s.y + model.modelfun(&s)*dt
}

pub fn improvedeuler<T: Add<Output = T> + Mul<f64, Output=T> + Copy, M: Model<T>>(model: &M, s: &State<T>, dt: f64) -> T {
    s.y + model.modelfun(&State{y: s.y + model.modelfun(&s)*(dt/2.0), t: s.t*(dt/2.0)})*dt
}

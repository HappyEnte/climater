use crate::simulation::{State, Model};

pub struct ClimateBalance {
    cs: f64,
    co: f64,
}

impl ClimateBalance {
    pub fn new(s: f64, c: f64, a: f64, e: f64, o: f64) -> Self {
        ClimateBalance {cs: s*(1.0-a)/(c*4.0), co: e*o/c}
    }
}

impl Model<f64> for ClimateBalance {
    fn modelfun(&self, s: &State<f64>) -> f64 {
            self.cs-self.co*(s.y*s.y*s.y*s.y)
    }
}

use std::fmt;
use std::ops::{Add, Mul};

use crate::simulation::{State, Model};

pub struct PredatorPrey {
   alpha: f64, // reproduktion der Beute
   beta: f64, // Fressrate der Räuber pro lebewesen
   delta: f64, // reproduktion der Räuber
   gamma: f64 // Sterberate der Räuber
}

impl PredatorPrey {
    pub fn new(alpha: f64, beta: f64, delta: f64, gamma: f64) -> Self {
        PredatorPrey {alpha: alpha, beta: beta, delta: delta, gamma: gamma}
    }
}

#[derive(Clone, Copy)]
pub struct Pair {
    pub prey: f64,
    pub predator: f64,
}

impl Model<Pair> for PredatorPrey {
    fn modelfun(&self, s: &State<Pair>) -> Pair {
        Pair { prey: self.alpha * s.y.prey - self.beta * s.y.prey * s.y.predator,
               predator: self.delta * s.y.predator * s.y.prey - self.gamma * s.y.predator }
    }
}

impl Default for PredatorPrey {
    fn default() -> Self {
        PredatorPrey{alpha: 1.0, beta: 0.2, delta: 0.2*0.5, gamma: 0.2}
    }
}

impl Default for Pair {
    fn default() -> Self {
        Pair{prey: 1 as f64, predator: 1.5 as f64}
    }

}

impl fmt::Display for Pair {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "(predators: {}, prey: {})", self.prey, self.predator)
    }
}

impl Mul<f64> for Pair {
    type Output = Pair;

    fn mul(self, other: f64) -> Pair {
        Pair {prey: self.prey*other, predator: self.predator*other}
    }
}

impl Add for Pair {
    type Output = Pair;

    fn add(self, other: Pair) -> Pair {
        Pair {prey: self.prey+other.prey, predator: self.predator+other.predator}
    }
}

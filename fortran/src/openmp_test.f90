program MAIN
  use omp_lib
  implicit none

  call exercise1()
  call exercise2()
  call worksharing()

contains

  subroutine exercise1 ()
    call omp_set_num_threads(4)
    !$omp parallel
    write(*,*) "Hello World from: ", omp_get_thread_num()
    !$omp end parallel
  end subroutine exercise1

  subroutine exercise2 ()
    integer(8) :: steps, i, k
    real(8) :: step, x, pi, sum

    steps = 100000
    step = 1.0/real(steps)

    !$omp parallel default(shared) private(i, x, k) shared(sum)
    k = (steps/omp_get_num_threads())
    do i=k*omp_get_thread_num(),k*(omp_get_thread_num()+1)
        x = (real(i) + 0.5) * step
        !$omp atomic
        sum = sum + 4.0/(1.0+x*x)
        !$omp end atomic
    end do
    !$omp end parallel
    pi = step * sum

    write(*,*) "Pi: ", pi

  end subroutine exercise2

  subroutine worksharing()
    integer :: j
    !$omp parallel
    !$omp do
    do j=0,10
       write(*,*) "j: ", j
    end do
    !$omp end do
    !$omp end parallel
  end subroutine worksharing
end program MAIN

program MAIN
  REAL(8), ALLOCATABLE :: A(:,:), x(:)
  REAL(8), ALLOCATABLE :: r(:)
  INTEGER :: n

  REAL(8) :: t1, t2

  WRITE(*,*) "Hello World!"

  n = 10000

  ALLOCATE(A(n,n))
  ALLOCATE(x(n))

  CALL RANDOM_NUMBER(A)
  CALL RANDOM_NUMBER(x)

  CALL CPU_TIME(t1)
  r = MULT(A,x)
  CALL CPU_TIME(t2)
  WRITE(*,*) 'MULT'
  WRITE(*,*) 'cputime = ', t2-t1

  CALL CPU_TIME(t1)
  r = MATMUL(A,x)
  CALL CPU_TIME(t2)
  WRITE(*,*) 'MATMUL'
  WRITE(*,*) 'cputime = ', t2-t1

  DEALLOCATE(A)
  DEALLOCATE(r)
  DEALLOCATE(x)

CONTAINS

function MULT(matA, vecX)
  REAL(8), intent(in) :: matA(:,:), vecX(:)

  REAL(8), ALLOCATABLE :: MULT(:)

  INTEGER :: m

  m = SIZE(vecX)

  ALLOCATE(MULT(m))

  do i=1,m
    do j=1,m
       MULT(i) = MULT(i) + matA(j,i)*vecX(j)
    ENDDO
  ENDDO

end function MULT

end program MAIN

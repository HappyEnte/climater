program main
  use omp_lib
  implicit none;

  real(8), allocatable :: B(:,:), D(:,:)
  real(8) :: t, maxT, alpha, epsilon, beta, delta, dt, k, tmp
  integer(8) :: x, y, step, maxSteps, n, i, j, ii, jj

  REAL(8) :: t1, t2

  ! Predator Prey parameters
  alpha = 2
  beta = 3
  epsilon = 1
  delta = 3
  maxT = 5000 ! 500 results in only NaN Final values
  n = 20
  k = 0.001
  maxSteps = 10 * (4*n*n*k*maxT)

  t = 0
  dt = maxT/maxSteps

  allocate(B(n*n, 2))

  ! ―――――――――――――――――――― create diffusion matrix ――――――――――――――――――――
  allocate(D(n*n,n*n))
  do ii=1, n*n
    do jj=1, n*n
      if ((ii == jj) .and. (ii == 1 .or. ii == n*n)) then
        D(ii, jj) = -2
      else if (ii == jj) then
        D(ii, jj) = -4
      else if (ii == jj-1 .or. ii == jj+1) then
        D(ii, jj) = 1
      else
        D(ii, jj) = 0
      endif
    enddo
  enddo
  D = k*n*n*D

  write(*,*) "maxSteps: ", maxSteps

  B(:,1) = 1 ! Set all x = 1
  !B(:,:) = 1 ! TODO replace with random number
  call random_number(B(:,2)) ! Set all y to random number


  CALL CPU_TIME(t1)
  do step=1,maxSteps
      t = t + dt
      call EULER_AT_POINT(B, x, y, t, .true.)
      !!$omp parallel copyin(bshared)
      !    !$omp do
      !    do x=1,n
      !        do y=1,n
      !            call EULER_AT_POINT(B, x, y, t, .true.)
      !        enddo
      !    enddo
      !    !$omp end do
      !!$omp end parallel
  enddo

  CALL CPU_TIME(t2)
  WRITE(*,*) 'value', B(:,2)
  WRITE(*,*) 'Finished after'
  WRITE(*,*) 'cputime = ', t2-t1

  open(unit=12, file='res.dat', status='replace', action='write')
  write(12, '(E20.6)') B
  close(unit=12)

  deallocate(B)

contains
  subroutine EULER_AT_POINT(space, x_cor, y_cor, time, diffusion)
    real(8) , intent(INOUT) :: space(:,:)
    real(8) , intent(IN) :: time
    integer(8) , intent(IN) :: x_cor, y_cor
    logical , intent(in) :: diffusion

    ! ――――――――――――――― Variante 1 - Direkte Berechnung ――――――――――――――
    !space(x_cor, y_cor, 1) = space(x_cor, y_cor, 1) + dt * (alpha    * space(x_cor, y_cor, 1) &
    !    - beta * space(x_cor, y_cor, 1) * space(x_cor, y_cor, 2))
    !space(x_cor, y_cor, 2) = space(x_cor, y_cor, 2) + dt * (-epsilon * space(x_cor, y_cor, 2) &
    !    + delta * space(x_cor, y_cor, 1) * space(x_cor, y_cor, 2))

    ! ―――――――――― Variante 2 - Berechnung via Matrix Produkt ――――――――――
    space(:,1) = space(:,1) + dt * (alpha * space(:,1) - beta * space(:,1) * space(:,2) + MATMUL(D,space(:,1)))
    space(:,2) = space(:,2) + dt * (-epsilon * space(:,2) + delta * space(:,1) * space(:,2) + MATMUL(D,space(:,2)))
  end subroutine

end program main

import numpy as np
import matplotlib.pyplot as plt
import math as m
import scipy.integrate as itr
from mpl_toolkits.mplot3d import Axes3D

# TODO Scipy Solver support


# Functions for runnning test cases
def runTestPlotIntermediate(testconf, initial, prefix):
    sim = simulation(create_lorenz(testconf),
                     0,     # Starttime
                     100,  # Endtime
                     np.array(initial))
    sim.run()
    data = sim.getResult()

    x_val = data[0]
    y_val = data[1]
    z_val = data[2]

    print(sim)

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot(x_val, y_val, z_val)
    fig.savefig("out/" + prefix +
                str(testconf) + "-" +
                str(initial) + "-" +
                sim.getUsedSolver() +
                '.png')
    plt.close(fig)


def runTests(testconfig, prefix=""):
    for conf in testconfig[0]:
        for val in testconfig[1]:
            runTestPlotIntermediate(conf, val, prefix)


class simulation:
    def __init__(self, modelfunction, t0, tmax, y0):
        self.f = modelfunction

        self.solver = self.improved_euler
        self.maxt = tmax
        self.maxsteps = 200000
        self.ys = np.zeros(self.maxsteps)
        self.xs = np.zeros(self.maxsteps)
        self.zs = np.zeros(self.maxsteps)

        self.t = t0
        self.y = y0
        self.dt = (self.maxt-t0)/self.maxsteps

    def run(self):
        for k in range(self.maxsteps):
            self.t += self.dt
            self.y = self.solver(self.f, self.t, self.y)

            self.xs[k] = self.y[0]
            self.ys[k] = self.y[1]
            self.zs[k] = self.y[2]

    def __str__(self):
        return "t: " + str(self.t) + "y: " + str(self.y)

    def setSolver(self, solverFunction):
        self.solver = solverFunction

    def improved_euler(self, f, t, y):
        return y + self.dt*f((self.dt/2.0)*t, y + (self.dt/2.0)*f(t, y))

    def euler(self, f, t, y):
        return y + self.dt*f(t, y)

    def getUsedSolver(self):
        return self.solver.__name__

    def getResult(self):
        return (self.xs, self.ys, self.zs)

    def getLast(self):
        return (self.xs[len(self.xs)-1],
                self.ys[len(self.ys)-1],
                self.zs[len(self.zs)-1])


def create_lorenz(conf):
    def lorenz(t, y):
        sig = conf[0]
        r = conf[1]
        b = conf[2]
        x0 = y[0]
        y0 = y[1]
        z0 = y[2]
        return np.array((sig*(y0-x0), (r-z0)*x0-y0, x0*y0-b*z0))
    return lorenz


#  ――――――――――――――――――――――――― Parareal method ――――――――――――――――――――――――
class parareal_simulation():
    def __init__(self, N, T, u0, modelfunction, reference):
        self.f = modelfunction
        self.N = N
        self.ref = reference
        self.T = T
        self.res = np.array(N, 3)
        self.res[0] = u0
        self.dT = T/N
        self.thres = 1
        self.finedt = self.dT*0.01
        self.coarse  =  self.dT

      # n x mal
#   0 ...|...|... T

    def coarse(self, y, t):

        return None

    def fine(self, y, t):
        return None

    def run(self):
        # initialise result
        t = 0
        for n in self.N:
            t += self.dT
            self.res[n+1] = self.coarse(self.res[n], t)

        # main loop
        ndelta = len(reference)/self.N
        run = True
        while run:
            maxerr = 0
            for k in range(self.N):
                for i in self.res[k]:
                    veclen = np.linalg.norm(self.res[k] - self.ref[m.floor(i*ndelta)])
                    if veclen > maxerr:
                        maxerr = veclen

            if maxerr < self.thres:
                run = False

            tmp = np.array(self.N, 3)
            for n in self.N:
                t += self.dT
                tmp[n+1] = self.coarse(tmp[n], t)

            self.res[n+1] = coarse(self.res[n]) + fine(self.res[n]) - coarse()

    def getResult(self):
        return None


#  ―――――――――――――――――――――――――――――― Tests ―――――――――――――――――――――――――――――
# a)
t1 = (10, 20, 8/3.0)
t1y0 = (10, 10, 10)
t1y1 = (0, 0, 0)
t1y2 = (-20, -2, 20)
test1 = ([t1], [t1y0, t1y1, t1y2])

# b)
t2 = (10, 28, 8/3.0)
t2y0 = (10, 10, 10)
t2y1 = (0, 0, 0)
t2y2 = (-20, -2, 20)
test2 = ([t2], [t2y0, t2y1, t2y2])

# c) TODO Where is the critical value?
t3y0 = (20, 2, 20)
test3 = ([], [t2y0], "test3")
for i in range(1, 1000, 20):
    test3[0].append((10, i/10.0, 8/3.0))

# d)
test4 = ([(10, 100.5, 8/3.0)], [(18.7, 29.9, 100)])

# e)
test5 = ([(10, 28, 8/3.0)], [(20, 5, -5)])

# TODO create Test cases for scipy
# itr.ode(create_lorenz(t2))

print("Task a)")
runTests(test1, "task_a/")
print("Task b)")
runTests(test2, "task_b/")
print("Task c)")
runTests(test3, "task_c/")
print("Task d)")
runTests(test4, "task_d/")
print("Task e)")
runTests(test5, "task_e/")

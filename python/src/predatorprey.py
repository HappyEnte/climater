import numpy as np
import time
import seaborn as sns
import scipy.sparse as sp
import matplotlib.pyplot as plt
import numpy as np


class predatorprey:
    def __init__(self, gridsize):
        "create new predatorprey model"

        self.n = gridsize

        self.alpha = 2
        self.beta = 3
        self.epsilon = 1
        self.delta = 3
        self.maxt = 5000
        self.k = 0.03
        self.maxsteps = 10*int(4*self.n*self.n*self.k*self.maxt)

        self.t = 0
        self.dt = self.maxt/self.maxsteps

        self.y_x = np.ones(self.n*self.n)
        self.y_y = np.random.rand(self.n*self.n)

        self.predator_results = []

        # self.predator_results =

        # no diffusioon 115200 steps ~ 3.33s

        # 115200 steps ~ 5.38s
        self.diffusion = sp.csr_matrix(self.__creatediffusion__(self.n*self.n))

        # 115200 steps ~ 5.39s
        # self.diffusion = sp.csc_matrix(self.__creatediffusion__(self.n*self.n))

        # 115200 steps ~ 7.22s
        # self.diffusion = (self.__creatediffusion__(self.n*self.n))

        # 115200 steps ~ horríblé, dont do that
        # self.diffusion = sp.lil_matrix(self.__creatediffusion__(self.n*self.n))

        print("simulation steps ", self.maxsteps)

    def __creatediffusion__(self, size):
        array = np.zeros((size, size))
        for i in range(0, size):
            for j in range(0, size):
                if i == j and (i == 0 or i == size-1):
                    array[i, j] = -2
                elif i == j:
                    array[i, j] = -4
                elif i == j-1 or i == j+1:
                    array[i, j] = 1
                else:
                    array[i, j] = 0
        return (self.k*(self.n*self.n))*array

    def __str__(self):
        # for i in range(self.y_y.size()/self.n):
            # for j in range(self.n):
                # print(" ", self.y_y[i+j])
            # print("\n")
        return self.y_y.__str__()

    def run(self):
        " mutates the predatorprey class"
        t = self.t
        for t in range(self.maxsteps):
            t += self.dt
            self.predatorprey(t)
            self.predator_results.append(list(self.y_y))

    def createPlot(self):
        plt.imshow(np.array(self.predator_results).transpose(), cmap='viridis', aspect='auto')
        plt.colorbar()
        plt.show()

    def predatorprey(self, t):
        self.y_x = self.y_x+self.dt*(self.alpha * self.y_x -
                            self.beta * self.y_x * self.y_y + self.diffusion.dot(self.y_x))
        self.y_y = self.y_y+self.dt*(-self.epsilon * self.y_y +
                            self.delta * self.y_x * self.y_y + self.diffusion.dot(self.y_y))

start = time.time()
pmodel = predatorprey(3)
pmodel.run()
print(pmodel)
end = time.time()
print("elapsed time: ", end - start)

pmodel.createPlot()

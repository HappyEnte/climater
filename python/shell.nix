with import <nixpkgs> {};
with python37Packages;

let
  python37mkl = python37Full.override {
    packageOverrides = sel: sup: {
      numpy = sup.numpy.override {
        blas = sup.pkgs.mkl;
      };
    };
  };

in stdenv.mkDerivation {
  name = "impurePythonEnv";

  src = null;

  buildInputs = [
    python37mkl
    python37Packages.virtualenv
    python37Packages.pip
    python37Packages.scipy
    python37Packages.elpy
    python37Packages.seaborn
    python37Packages.python-language-server
    python37Packages.matplotlib

    openblas
  ];

  shellHook = ''
    # set SOURCE_DATE_EPOCH so that we can use python wheels
    #SOURCE_DATE_EPOCH=$(date +%s)
    #virtualenv --no-setuptools venv
    #export PATH=$PWD/venv/bin:$PATH
    #pip install -r requirements.txt
  '';
}

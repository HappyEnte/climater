## Requirements

- `rustup`
- `cargo`
- `gnuplot` built with some graphical terminal (WxGTK, QT).

## Running
```
cargo run --release
```
